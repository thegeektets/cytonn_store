import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import {Product} from '../../product/models/product';
import { Cart, CartItem } from '../models/cart';


@Component({
    selector: 'as-cart',
    templateUrl: 'app/cart/templates/cart.html',
    styleUrls: [
        'app/cart/styles/cart.css'
    ]
})

export class CartComponent implements OnInit {
    public cart: Cart;
    public loading: boolean = true;
    public quantityOptions: number[] = [];
    private placeholder = 'https://firebasestorage.googleapis.com/v0/b/cytonn-store.appspot.com/o/cytonn-store-default.png?alt=media&token=5aaa300e-b735-48da-b67b-6bcba72abd7b';

    constructor(
        private _cart: CartService
    ) {
        let i = 0;
        while (i < 10) {
            i++;
            this.quantityOptions.push(i);
        }
    }
    ngOnInit() {
        this._cart.getList().subscribe((res) => {
            this.cart = res;
            this.loading = false;
        });
    }
    raiseQuantity(product_id: number, chosen_attributes: any) {
        this._cart.add(product_id, chosen_attributes).subscribe((res) => {
            this.cart = res;
        });
    }
    lowerQuantity(cartItem: CartItem) {
        this._cart.setQuantity(cartItem.id, cartItem.quantity - 1).subscribe((res) => {
            this.cart = res;
        });
    }
    removeItem(cartItem: CartItem) {
        this._cart.remove(cartItem.id).subscribe((res) => {
            this.cart = res;
        });
    }
    updateQuantity(newValue: number, cartItem: CartItem) {
        this._cart.setQuantity(cartItem.id, newValue).subscribe((res) => {
            this.cart = res;
        });
    }
    errorHandler(event) {
       event.target.src = this.placeholder;
    }
}
