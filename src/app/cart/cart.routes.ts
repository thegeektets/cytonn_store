import {CartComponent} from './components/cart.component';

export const CartRoutes = [
    { path: 'cart',  component: CartComponent }
];
