import {ProfileComponent} from './components/profile.component/';

export const AccountRoutes = [
    { path: 'user/profile', component: ProfileComponent}
];
