import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { ProductService } from '../../product/services/product.service';
import { CategoryService } from '../../product/services/category.service';
import { Product } from '../../product/models/product';
import { SessionService } from '../../services/SessionService';
import { ToasterService } from 'angular2-toaster';
import { ListResponse } from '../../bases/models/ListResponse';

@Component({
    selector: 'as-admin-list-product',
    templateUrl: 'app/admin/templates/admin_list_product.html',
    styleUrls: [
        'app/admin/styles/admin_list_product.css'
    ]
})


export class AdminListProductComponent implements OnInit {
    public errors: Object;
    public loading: boolean = true;
    public product: Product;
    public productsResponse: ListResponse;
    private oid: string;
    private productForm: FormGroup;
    private searchterm: string = '';
    constructor(
        private fb: FormBuilder,
        private _sessionService: SessionService,
        private _productService: ProductService,
        private _categoryService: CategoryService,
        private _toasterService: ToasterService
                ) {
        }
    ngOnInit() {
        this.getProducts();
    }

    getProducts() {
        this.loading = true;
        this._productService.getList().subscribe((res) => {
            this.productsResponse = res;
            this.loading = false;
        });
    }

     search(searchTerm: string) {
        this.loading = true;
        let params = {};
        if (searchTerm !== '') {
            let field = 'search';
            params[field] = searchTerm;
        }
        this._productService.getList(params).subscribe((res) => {
            this.productsResponse = res;
            this.loading = false;
        });
    }

    searchKeyPressed($event) {
        if ($event.keyCode === 13) {
            this.search($event.target.value);
        }
    }

    deleteProduct($product_id) {
        this.loading = true;
        this._productService.delete($product_id).subscribe((res) => {
            this.getProducts();
            this._toasterService.pop('success', 'Product deleted');
            this.loading = false;
        });
        return false;
    }
}
